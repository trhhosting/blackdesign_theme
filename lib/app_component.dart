import 'package:angular/angular.dart';

// Header title bar
// Footer title bar
// Hero Sections Top of the fold
// Blog Components
// Blog section
// Contact Us
// Features
// Headers
// Pricing

@Component(
  selector: 'app',
  templateUrl: 'app_component.html',
  styleUrls: ['app_component.css'],
  directives: const [
    coreDirectives,
  ],
)
class AppComponent {
  var name = 'Now AngularDart UI Kit';
}
